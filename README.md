# redhat-mfp-setup
Ansible playbooks for installing IBM Installation Manager, WAS Liberty, IBM Java and MobileFirst Platform.
Setup Application Center, Operational Analytics and Mobile Hosting Server.
 
# Roles
Downloads the binaries from the jfrog repository, transfers and installs on the host VMs.
The credentials for connecting to jfrog repository is to be updated with the SSO token generated.

Below are the roles used in this playbook

* redhat-yum-prerequisites -- Installs the basic pre-requisites required for executing this playbook.
* redhat-mfp-iim -- Installs IIM installation Manager.
* redhat-mfp-was -- Installs WAS Liberty, IBM Java and Fix packages for WAS Liberty and IBM Java.
* redhat-mfp-appcenter -- Installs MFP and configures appcenter.
* redhat-mfp-oa -- Installs MFP and configures Operational Analytics.
* redhat-mfp-fs -- Installs MFP and configures Mobile Hosting Platform.

The roles are to be executed in the order as per the playbook, as the components are dependant on the availability of each other.
 
# Getting Started
## Pre-requisites
* Place IBM Installation Manager binary 1.8.4, WAS Liberty 8.5.5, Java 7.0 and MFP 8.0 for RHEL 7.6 in Artifactory.
* Update Host and connection details in hosts file.
* Repo details and DB IP to be updated in playbook vars.
* IBM DB2 should be installed and empty databases listed below has to be created.
    - Three databases WLADMIN, APPCNTR and WRKLGHT
    - The database APPCNTR is a pre-requisite for AppCenter deployment and configuration.
    - The database WRKLGHT is a pre-requisite for Mobile Hosting Sever.
    - The user wluser should have access to all the 3 databases and all the db objects created as part of the application deployment.
 
## Running Playbook
ansible-playbooks -i <hosts-file-path> playbooks/mfp/redhat-mfp-setup.yml --extra-vars '{"ac_config_deploy":"true", "oa_config_deploy":"true", "fs_config_deploy":"true"}'

* The variables ac_config_deploy, oa_config_deploy and fs_config_deploy can be set to true | false.
* If true, the configuration of respective components will be executed and deployed.
* All config should be set to true for first time installation and deployment.
* If set to false, only the installation of MFP and its components would be executed.